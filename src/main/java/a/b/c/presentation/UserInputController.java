package a.b.c.presentation;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import a.b.c.application.user.UserService;
import a.b.c.domain.code.GenderCd;
import a.b.c.domain.user.UserEntity;
import a.b.c.infrastructure.entitycrud.EntityInputController;

@Named
@ViewScoped
public class UserInputController extends EntityInputController<UserEntity, String, UserService>
        implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * ユーザーID
     */
    private String userId;

    @Inject
    transient UserService service;

    public GenderCd[] get性別() {
        return GenderCd.values();
    }

    @Override
    protected UserService getService() {
        return service;
    }

    @Override
    protected String getEntityId() {
        return getUserId();
    }

    /**
     * ユーザーIDを返却する。
     * 
     * @return ユーザーID
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * ユーザーIDを設定する。
     * 
     * @param userId
     *            ユーザーID
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
