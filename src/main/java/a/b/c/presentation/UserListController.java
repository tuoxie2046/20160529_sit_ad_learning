package a.b.c.presentation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import a.b.c.application.user.UserService;
import a.b.c.domain.user.UserEntity;
import a.b.c.infrastructure.entitycrud.EntityListController;

@Named
@RequestScoped
public class UserListController extends EntityListController<UserEntity, String, UserService> {

    @Inject
    UserService service;

    @Override
    protected UserService getService() {
        return service;
    }
}
