package a.b.c.application.user;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import a.b.c.domain.user.UserEntity;
import a.b.c.domain.user.UserRepository;
import a.b.c.infrastructure.data.jpa.BaseRepository;
import a.b.c.infrastructure.entitycrud.BaseService;

/**
 * このクラスは、ユーザーマスターのサービスです。
 * 
 * @author SIToolkit
 **/
@ApplicationScoped
@Transactional
public class UserService extends BaseService<UserEntity, String> {

    @Inject
    UserRepository dao;

    @Override
    public BaseRepository<UserEntity, String> getDao() {
        return dao;
    }

}
